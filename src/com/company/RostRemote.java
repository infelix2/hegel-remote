package com.company;

import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.TimeoutException;


public class RostRemote implements HegelController {


    private HashMap<Integer,String> inputs;
    private final int port = 50001;
    private MessageSender ms;

    public RostRemote(String host) throws IOException {
        ms = new MessageSender(host, port);
        inputs = new HashMap<>();
        inputs.put(1,"Balanced");
        inputs.put(2,"Analog 1");
        inputs.put(3,"Analog 2");
        inputs.put(4,"Coaxial");
        inputs.put(5,"Optical 1");
        inputs.put(6,"Optical 2");
        inputs.put(7,"Optical 3");
        inputs.put(8,"USB");
        inputs.put(9,"Network");
    }

    public HashMap<Integer, String> getInputs() {
        return inputs;
    }

    public String getInputByKey(int key){
        return inputs.get(key);
    }

    @Override
    public Integer volumeUp() throws IOException, TimeoutException {

        if (getVolume() == 100) return 100;
        return Integer.parseInt(ms.sendMessage("-v.u\r\n").split("\\.")[1]);
//        return ms.sendMessage("-v.u\r\n");
    }

    @Override
    public Integer volumeDown() throws IOException, TimeoutException {
        if (getVolume() == 0) return 0;
        return Integer.parseInt(ms.sendMessage("-v.d\r\n").split("\\.")[1]);
    }
    @Override
    public Integer getVolume() throws IOException, TimeoutException {
        return Integer.parseInt(ms.sendMessage("-v.?\r\n").split("\\.")[1]);
    }

    @Override
    public Integer setVolume(int volume) throws IOException, TimeoutException {
        int current = getVolume();

        if (volume == current) return current;
        return Integer.parseInt(ms.sendMessage("-v." + volume + "\r\n").split("\\.")[1]);
    }

    @Override
    public Boolean powerOff() throws IOException, TimeoutException {
        return ms.sendMessage("-p.0\r\n").split("\\.")[1].equals("1");
    }

    @Override
    public Boolean powerOn() throws IOException, TimeoutException {
        return ms.sendMessage("-p.1\r\n").split("\\.")[1].equals("1");
    }

    @Override
    public Boolean isPowered() throws IOException, TimeoutException {
        return ms.sendMessage("-p.?\r\n").split("\\.")[1].equals("1");
    }

    @Override
    public Boolean togglePower() throws IOException, TimeoutException {
        if (isPowered())
            return powerOff();
        else
            return powerOn();
    }

    @Override
    public Boolean isMuted() throws IOException, TimeoutException {
        return ms.sendMessage("-m.?\r\n").split("\\.")[1].equals("1");
    }

    @Override
    public Boolean mute() throws IOException, TimeoutException {
        if (isMuted()) return true;
        return ms.sendMessage("-m.1\r\n").split("\\.")[1].equals("1");
    }

    @Override
    public Boolean unMute() throws IOException, TimeoutException {
        if (!isMuted()) return false;
        return ms.sendMessage("-m.0\r\n").split("\\.")[1].equals("1");
    }

    @Override
    public Boolean toggleMute() throws IOException, TimeoutException {
        if (isMuted()){
            unMute();
            return true;
        }
        else
        {
            mute();
            return false;
        }
    }

    @Override
    public Integer getInput() throws IOException, TimeoutException {
        return Integer.parseInt(ms.sendMessage("-i.?\r\n").split("\\.")[1]);
    }

    @Override
    public String getInputName() throws IOException, TimeoutException {
        return inputs.get(getInput());
    }

    @Override
    public Integer setInput(int input) throws IOException, TimeoutException {
        return Integer.parseInt(ms.sendMessage("-i." + input + "\r\n").split("\\.")[1]);
    }

}
