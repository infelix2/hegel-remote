package com.company;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;

public class MessageSender {
    private Socket socket;
    private String response;

    public MessageSender(String host, int port) throws IOException {
        InetAddress address = InetAddress.getByName(host);
        int port1 = port;

        socket = new Socket(address, port);

    }


    public String sendMessage(String msg) throws IOException, UnsupportedOperationException {
        OutputStream os = socket.getOutputStream();
        OutputStreamWriter osw = new OutputStreamWriter(os);
        BufferedWriter bw = new BufferedWriter(osw);

        String sendMessage = msg + "\n";
        bw.write(sendMessage);
        bw.flush();

        InputStream is = socket.getInputStream();
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);
        socket.setSoTimeout(2000);
        try{
            response = br.readLine();
        }
        catch (SocketTimeoutException e){
            System.out.println("TIMED OUT");
            e.printStackTrace();
            response = "e.-1";
        }
//        System.out.println(response);
        if (response.split("\\.")[0].equals("-e")) {
            try {
                throw new UnsupportedOperationException("Hegel did not understand your message:" + msg);
            } catch (UnsupportedOperationException e) {
                e.printStackTrace();
            }
        }
        return response;

    }

    public String getResponse() {
        return response;
    }
}
