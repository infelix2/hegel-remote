package com.company;



import java.io.IOException;
import java.util.concurrent.TimeoutException;

interface HegelController {

    Integer volumeUp() throws IOException, ClassNotFoundException, TimeoutException;

    Integer volumeDown() throws IOException, ClassNotFoundException, TimeoutException;

    Integer getVolume() throws IOException, ClassNotFoundException, TimeoutException;

    Integer setVolume(int volume) throws IOException, ClassNotFoundException, TimeoutException;

    Boolean powerOff() throws IOException, ClassNotFoundException, TimeoutException;

    Boolean powerOn() throws IOException, ClassNotFoundException, TimeoutException;

    Boolean isPowered() throws IOException, ClassNotFoundException, TimeoutException;

    Boolean togglePower() throws IOException, ClassNotFoundException, TimeoutException;

    Boolean isMuted() throws IOException, ClassNotFoundException, TimeoutException;

    Boolean mute() throws IOException, ClassNotFoundException, TimeoutException;

    Boolean unMute() throws IOException, ClassNotFoundException, TimeoutException;

    Boolean toggleMute() throws IOException, ClassNotFoundException, TimeoutException;

    Integer getInput() throws IOException, TimeoutException;

    String getInputName() throws IOException, TimeoutException;

    Integer setInput(int input) throws IOException, TimeoutException;


}